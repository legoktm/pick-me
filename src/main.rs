/**
Suggests patches to cherry-pick/backport to stable release branches for MediaWiki
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::{anyhow, Result};
use mailparse::{MailHeaderMap, ParsedMail};
use serde::Deserialize;
use std::cmp::Reverse;
use std::{fmt, sync::Arc};
use tempfile::tempdir;
use tokio::{fs, process::Command, sync::Semaphore};

/// Commmits to ignore
const SKIP_SUBJECTS: [&str; 2] = [
    "Update git submodules",
    "Localisation updates from https://translatewiki.net.",
];

/// Authors to ignore
const SKIP_AUTHORS: [&str; 1] = ["[BOT] libraryupgrader"];

/// First commit after REL1_36 was branched
const BRANCHPOINT: &str = "c791a1a4a9322c224fc77183fab9fed5d2d7a13c";
/// core git clone
const CHECKOUT: &str = "/home/km/tmp/core";

/// Status of the patch in the table
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Status {
    CherryPicked,
    Applies,
    Conflicts,
}

impl fmt::Display for Status {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            Status::CherryPicked => "cherry-picked",
            Status::Applies => "applies",
            Status::Conflicts => "conflicts",
        };
        write!(f, "{}", msg)
    }
}

#[derive(Debug, Copy, Clone)]
struct Branches {
    /// REL1_31
    one: Status,
    /// REL1_35
    five: Status,
    /// REL1_36
    six: Status,
}

impl Branches {
    fn done(&self) -> bool {
        self.has(Status::CherryPicked) == 3
    }

    fn has(&self, status: Status) -> usize {
        let mut count = 0;
        if self.one == status {
            count += 1;
        }
        if self.five == status {
            count += 1;
        }
        if self.six == status {
            count += 1;
        }
        count
    }
}

#[derive(Debug)]
struct Commit {
    /// abbreviated sha1
    sha1: String,
    /// patch ile of commit
    patch: String,
}

impl Commit {
    /// From the abbreviated sha1, get a new commit object
    async fn new(sha1: &str) -> Result<Self> {
        let output = Command::new("git")
            .args(&["show", "--format=email", sha1])
            .current_dir(CHECKOUT)
            .output()
            .await?;
        // TODO: check exit code
        Ok(Commit {
            sha1: sha1.to_string(),
            patch: String::from_utf8(output.stdout)?,
        })
    }

    fn parsed(&self) -> Result<ParsedMail> {
        Ok(mailparse::parse_mail(self.patch.as_bytes())?)
    }

    /// Get a git header.
    /// Cannot be used on trailers/footers like Change-Id or Bug
    fn header(&self, name: &str) -> Result<String> {
        Ok(self
            .parsed()?
            .get_headers()
            .get_first_header(name)
            .ok_or_else(|| anyhow!("Missing {} header", name))?
            .get_value())
    }

    /// Author name, without email address
    fn author(&self) -> Result<String> {
        let author = self.header("From")?;
        match author.split_once('<') {
            Some((name, _)) => Ok(name.trim().to_string()),
            None => Ok(author),
        }
    }

    /// First line of the commit message
    fn subject(&self) -> Result<String> {
        self.header("Subject")
            .map(|s| s.trim_start_matches("[PATCH] ").to_string())
    }

    /// Full Change-Id value. Errors if it can't find one
    fn change_id(&self) -> Result<String> {
        for line in self.patch.split('\n') {
            if line.starts_with("Change-Id:") {
                return Ok(line.trim_start_matches("Change-Id: ").to_string());
            }
        }

        Err(anyhow!("Unable to find Change-Id footer"))
    }

    fn bugs(&self) -> Result<Vec<String>> {
        Ok(self
            .patch
            .split('\n')
            .filter_map(|line| {
                if line.starts_with("Bug: ") {
                    Some(line.trim_start_matches("Bug: ").to_string())
                } else {
                    None
                }
            })
            .collect())
    }

    /// Lines added and removed
    fn size(&self) -> (u32, u32) {
        let mut plus = 0;
        let mut minus = 0;
        for line in self.patch.split('\n') {
            // TODO: what if a commit message line starts with a + or -?
            if line.starts_with('+') && !line.starts_with("+++") {
                plus += 1;
            } else if line.starts_with('-') && !line.starts_with("---") {
                minus += 1;
            }
        }

        (plus, minus)
    }

    /// is the commit cherry-picked onto the given branch
    async fn cherry_picked_on(&self, branch: &str) -> Result<bool> {
        // FIXME: is there a faster way to do this? Could we limit the
        // scope of commits to look through?
        let output = Command::new("git")
            .args(&[
                "log",
                "--exit-code",
                &format!("--grep=Change-Id: {}", self.change_id()?),
                &format!("origin/{}", branch),
            ])
            .current_dir(CHECKOUT)
            .output()
            .await?;
        match output.status.code() {
            // git log exits with code 1 if it finds a commit, 0 if not
            Some(code) => Ok(code == 1),
            None => Err(anyhow!("git log terminated by signal")),
        }
    }

    async fn branch_status_inner(
        &self,
        branch: &str,
        head: &str,
    ) -> Result<Status> {
        Ok(if self.cherry_picked_on(branch).await? {
            Status::CherryPicked
        } else if self.applies_on(head).await? {
            Status::Applies
        } else {
            Status::Conflicts
        })
    }

    /// list of branches its been cherry-picked on
    async fn branch_status(&self, heads: &Heads) -> Result<Branches> {
        let (one, five, six) = tokio::try_join!(
            self.branch_status_inner("REL1_31", &heads.one),
            self.branch_status_inner("REL1_35", &heads.five),
            self.branch_status_inner("REL1_36", &heads.six),
        )?;
        Ok(Branches { one, five, six })
    }

    /// See if this commit can be naively cherry-picked
    /// TODO: try dropping release notes to improve chances
    async fn applies_on(&self, head: &str) -> Result<bool> {
        let dir = tempdir()?;
        let output = Command::new("git")
            .args(&[
                "clone",
                "--shared",
                "--local",
                CHECKOUT,
                dir.path().to_str().unwrap(),
            ])
            .output()
            .await?;
        if !output.status.success() {
            return Err(anyhow!("Unable to create tempdir clone"));
        }
        // Switch to the right branch
        let output = Command::new("git")
            .args(&["checkout", head])
            .current_dir(dir.path())
            .output()
            .await?;
        if !output.status.success() {
            return Err(anyhow!("Failed to checkout {}", head));
        }
        let output = Command::new("git")
            .args(&["cherry-pick", &self.sha1])
            .current_dir(dir.path())
            .output()
            .await?;
        Ok(output.status.success())
    }
}

/// Identifies the HEAD of a branch, so the sha1 can
/// be used directly in other clones
async fn get_head(branch: &str) -> Result<String> {
    let output = Command::new("git")
        .args(&[
            "log",
            "--oneline",
            "--format=%H",
            "-n1",
            &format!("origin/{}", branch),
        ])
        .current_dir(CHECKOUT)
        .output()
        .await?;
    if !output.status.success() {
        return Err(anyhow!("Unable to get HEAD of branch {}", branch));
    }

    Ok(String::from_utf8(output.stdout)?.trim().to_string())
}

/// Represents the HEAD of each branch
#[derive(Debug, Clone)]
struct Heads {
    one: String,
    five: String,
    six: String,
}

/// Given a sha1, build a Row object
async fn build_row(sha1: &str, heads: &Heads) -> Result<Option<Row>> {
    let commit = Commit::new(sha1).await?;
    Row::new(&commit, heads).await
}

/// Represents a wikitable row
#[derive(Debug)]
struct Row {
    sha1: String,
    subject: String,
    author: String,
    branches: Branches,
    bugs: Vec<String>,
    plus: u32,
    minus: u32,
}

impl Row {
    async fn new(commit: &Commit, heads: &Heads) -> Result<Option<Self>> {
        let subject = commit.subject()?;
        if matches_any(&SKIP_SUBJECTS, &subject) {
            return Ok(None);
        }
        let author = commit.author()?;
        if matches_any(&SKIP_AUTHORS, &author) {
            return Ok(None);
        }
        let branches = commit.branch_status(heads).await?;
        if branches.done() {
            return Ok(None);
        }
        dbg!(&commit.sha1, &subject, &branches);
        let (plus, minus) = commit.size();
        Ok(Some(Self {
            sha1: commit.sha1.to_string(),
            subject,
            author,
            branches,
            bugs: commit.bugs()?,
            plus,
            minus,
        }))
    }

    /// Format the row into wikitext
    /// TODO: Use parsoid-rs and build HTML here instead
    fn text(&self) -> String {
        let bugs = {
            let list: Vec<_> = self
                .bugs
                .iter()
                .map(|id| format!("[[phab:{id}|{id}]]", id = id))
                .collect();
            list.join(", ")
        };
        format!(
            r#"|-
|[[gerrit:q/{sha1}|[{sha1}] {subject}]] ({author})
|{bugs}
|<nowiki/>+{plus}/-{minus}
|{score}
|{one}
|{five}
|{six}"#,
            sha1 = &self.sha1,
            subject = &self.subject,
            author = &self.author,
            one = &self.branches.one,
            five = &self.branches.five,
            six = &self.branches.six,
            bugs = bugs,
            plus = &self.plus,
            minus = &self.minus,
            score = self.score(),
        )
    }

    /// Use heuristics to score the commit, the higher the score,
    /// the more likely it should be backported.
    /// Note: do not subtract points, just invert the condition
    /// and add to everything else indead
    fn score(&self) -> usize {
        let mut score = 0;
        // 20 points for each merged cherry-pick
        score += 20 * self.branches.has(Status::CherryPicked);
        // 10 points for each branch that cleanly applies
        score += 10 * self.branches.has(Status::Applies);
        // 10 points per bug fixed
        score += self.bugs.len() * 10;
        // 100 points if the subject contains "SECURITY"
        if self.subject.contains("SECURITY") {
            score += 100;
        }
        // 10 points if the subject doesn't start with "build :"
        if !self.subject.starts_with("build: ") {
            score += 10;
        }
        // Smaller diffs are better, add based on size.
        // < 100 lines = 15 points
        // 100+ lines = 10 points
        // 500+ lines = 5 points
        // 1000+ lines = 0 points
        let lines = self.plus + self.minus;
        if lines < 100 {
            score += 15;
        } else if (100..500).contains(&lines) {
            score += 10;
        } else if (500..1000).contains(&lines) {
            score += 5;
        }
        score
    }
}

/// Return `true` if `thing` is a substring of any item in the `list`
fn matches_any(list: &[&str], thing: &str) -> bool {
    for item in list {
        if item.contains(thing) {
            return true;
        }
    }

    false
}

// Bot config
#[derive(Deserialize)]
struct Config {
    auth: BotPassword,
}

#[derive(Deserialize)]
struct BotPassword {
    username: String,
    password: String,
}

async fn load_config() -> Result<Config> {
    let path = dirs_next::home_dir().unwrap().join(".config.toml");
    let contents = fs::read_to_string(path).await?;
    Ok(toml::from_str(&contents)?)
}

async fn save_page(text: &str) -> Result<()> {
    let config = load_config().await?;
    let api = mwapi::Client::bot_builder("https://www.mediawiki.org/w/api.php")
        .set_botpassword(&config.auth.username, &config.auth.password)
        .build()
        .await?;
    api.post_with_token(
        "csrf",
        &[
            ("action", "edit"),
            ("title", "User:Legoktm/pickme"),
            ("text", text),
            ("summary", "Updating table"),
        ],
    )
    .await?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    // Update git repo
    let status = Command::new("git")
        .args(&["fetch", "origin"])
        .current_dir(CHECKOUT)
        .status()
        .await?;
    if !status.success() {
        return Err(anyhow!("Unable to git fetch origin"));
    }
    let output = Command::new("git")
        .args(&[
            "log",
            &format!("origin/master...{}", BRANCHPOINT),
            "--oneline",
            "--format=%h",
            "--no-merges",
        ])
        .current_dir(CHECKOUT)
        .output()
        .await?;
    // TODO: check exit code
    let heads = {
        let (one, five, six) = tokio::try_join!(
            get_head("REL1_31"),
            get_head("REL1_35"),
            get_head("REL1_36"),
        )?;
        Heads { one, five, six }
    };
    let text = String::from_utf8(output.stdout)?;
    let mut wikitext = vec![r#"{| class="wikitable sortable"
!Commit
!Bugs
!Size
!Score
!REL1_31
!REL1_35
!REL1_36"#
        .to_string()];
    let semaphore = Arc::new(Semaphore::new(4));
    let mut handles = vec![];
    for sha1 in text.trim().split('\n') {
        let heads = heads.clone();
        let semaphore = semaphore.clone();
        let sha1 = sha1.to_string();
        handles.push(tokio::spawn(async move {
            let _permit = semaphore.acquire().await.unwrap();
            build_row(&sha1, &heads).await
        }));
    }
    let mut rows = vec![];
    for handle in handles {
        if let Some(row) = handle.await?? {
            rows.push(row);
        }
    }
    rows.sort_by_key(|a| Reverse(a.score()));
    for row in rows {
        wikitext.push(row.text());
    }
    wikitext.push("|}".to_string());
    let text = wikitext.join("\n");
    save_page(&text).await?;
    println!("Updated [[User:Legoktm/pickme]]");
    Ok(())
}
